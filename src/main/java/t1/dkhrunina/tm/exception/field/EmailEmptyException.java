package t1.dkhrunina.tm.exception.field;

public class EmailEmptyException extends AbstractFieldException {

    public EmailEmptyException() {
        super("Error: email is empty");
    }

}