package t1.dkhrunina.tm.exception.system;

public class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error: command is not supported");
    }

    public CommandNotSupportedException(final String command) {
        super("Error: command \"" + command + "\" is not supported");
    }

}