package t1.dkhrunina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import t1.dkhrunina.tm.model.Task;

public interface IProjectTaskService {

    @NotNull
    Task bindTaskToProject(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    void removeAllByUserId(@NotNull String userId);

    void removeProjectById(@NotNull String userId, @Nullable String projectId);

    void removeProjectByIndex(@NotNull String userId, @Nullable Integer index);

    @NotNull
    Task unbindTaskFromProject(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

}