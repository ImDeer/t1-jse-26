package t1.dkhrunina.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    private static final String NAME = "about";

    @NotNull
    private static final String DESCRIPTION = "Show application info.";

    @Override
    public void execute() {
        System.out.println("\n[APPLICATION NAME]");
        System.out.println("Name: " + getPropertyService().getApplicationName());

        System.out.println("\n[AUTHOR]");
        System.out.println("Name: " + getPropertyService().getAuthorName());
        System.out.println("Email: " + getPropertyService().getAuthorEmail());

        System.out.println("\n[GIT]");
        System.out.println("Branch: " + getPropertyService().getGitBranch());
        System.out.println("Commit ID: " + getPropertyService().getGitCommitId());
        System.out.println("Commit time: " + getPropertyService().getGitCommitTime());
        System.out.println("Commit message: " + getPropertyService().getGitCommitMessage());
        System.out.println("Committer: " + getPropertyService().getGitCommitterName());
        System.out.println("Committer email: " + getPropertyService().getGitCommitterEmail());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}