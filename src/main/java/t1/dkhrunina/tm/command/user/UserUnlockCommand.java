package t1.dkhrunina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "u-unlock";

    @NotNull
    private static final String DESCRIPTION = "Unlock user by login .";

    @Override
    public void execute() {
        System.out.println("[Unlock user]");
        System.out.println("Enter login: ");
        @NotNull final String login = TerminalUtil.nextLine();
        getAuthService().unlockUserByLogin(login);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}