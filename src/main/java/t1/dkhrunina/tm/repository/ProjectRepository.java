package t1.dkhrunina.tm.repository;

import t1.dkhrunina.tm.api.repository.IProjectRepository;
import t1.dkhrunina.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

}